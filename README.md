## Instructions
#### Install requirements
1. Nextflow
2. Docker
#### Get workflow and Docker container
```
git clone https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/resfinder-4.git
docker pull bioinformant/resfinder-4:latest
```
#### Running the pipeline
Example here for Klebsiella pneumoniae
```
nextflow run /path/to/resfinder/main.nf --fastas "/path/to/*.fasta" --output_dir /path/to/output_dir --species "Klebsiella pneumoniae" -resume
```
Optionally the `--coverage_threshold` and `--identity_threshold` can be used

params.output_dir = false

process RUN_RESFINDER_FASTQS {
  tag "$id"
  publishDir params.output_dir, mode: 'copy'
  
  input:
    tuple(val(id), path(reads))
    val(species)
    val(coverage_threshold)
    val(identity_threshold)
  output:
    path("${id}.tab.tsv"), emit: tsv_result
    path("${id}.table.txt")
    path("${id}.pheno.txt")
  script:
    """
    run_resfinder.py -o . -s "${species}" -l ${coverage_threshold} -t ${identity_threshold} --acquired --point -ifq ${reads}
    # add column for id
    awk '{FS=OFS="\\t";print "${id}", \$0}' ResFinder_results_tab.txt > ${id}.tab.tsv
    # move other files
    mv ResFinder_results_table.txt ${id}.table.txt
    mv pheno_table.txt ${id}.pheno.txt
    """
}


process RUN_RESFINDER_FASTAS {
  tag "$id"
  publishDir params.output_dir, mode: 'copy'

  input:
    tuple(val(id), path(fasta))
    val(species)
    val(coverage_threshold)
    val(identity_threshold)
  output:
    path("${id}.tab.tsv"), emit: tsv_result
    path("${id}.table.txt")
    path("${id}.pheno.txt")
  script:
    """
    run_resfinder.py -o . -s "${species}" -l ${coverage_threshold} -t ${identity_threshold} --acquired --point -ifa ${fasta}
    # add column for id
    awk '{FS=OFS="\\t";print "${id}", \$0}' ResFinder_results_tab.txt > ${id}.tab.tsv
    # move other files
    mv ResFinder_results_table.txt ${id}.table.txt
    mv pheno_table.txt ${id}.pheno.txt
    """
}

process COMBINE_RESFINDER_RESULTS {
    publishDir params.output_dir, mode: 'copy'
    input:
      path(result_files)

    output:
      path("combined_results.tsv")

    script:
      """
        echo "ID\tResistance gene Identity\tAlignment Length/Gene Length\tCoverage\tPosition in reference\tContig\tPosition in contig\tPhenotype\tAccession no." > combined_results.tsv
        for file in ${result_files}; do
          tail -n +2 \${file} >> combined_results.tsv
        done
      """
}
